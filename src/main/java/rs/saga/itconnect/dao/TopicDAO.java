package rs.saga.itconnect.dao;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import rs.saga.itconnect.entity.Topic;

/**
 * Created by mmilcic on 3/30/2018.
 * Repository
 */
@Repository
public interface TopicDAO extends CrudRepository<Topic, Integer> {
}
