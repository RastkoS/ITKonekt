package rs.saga.itconnect.service;

import rs.saga.itconnect.entity.Speaker;

public interface ITSpeakerService {

    Speaker getSpeakerByID(Integer id);

    void saveOrUpdateSpeaker(Speaker Speaker);

    Iterable<Speaker> getSpeakers();

    void deleteSpeaker(Integer id);

    void deleteAllSpeakers();
    
    
}
