package rs.saga.itconnect.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import rs.saga.itconnect.dao.SpeakerDAO;
import rs.saga.itconnect.entity.Speaker;
import rs.saga.itconnect.exception.SpeakerNotExistException;

import java.util.Optional;

/**
 * Created by mmilcic on 4/4/2018.
 * Service class
 */

@Service
public class SpeakerServiceImpl implements ITSpeakerService{

    private SpeakerDAO speakerDAO;

    @Autowired
    public SpeakerServiceImpl(SpeakerDAO speakerDAO){
        this.speakerDAO = speakerDAO;
    }

    public void saveOrUpdateSpeaker(Speaker speaker){
        speakerDAO.save(speaker);
    }

    public Speaker getSpeakerByID(Integer id){
        Optional<Speaker> speaker = speakerDAO.findById(id);
        if(speaker.isPresent()){
            return speaker.get();
        }else{
            throw new SpeakerNotExistException("Speaker does not exist");
        }
    }

    public Iterable<Speaker> getSpeakers(){
        return speakerDAO.findAll();
    }

    public void deleteSpeaker(Integer id){
        speakerDAO.findById(id).ifPresent(topic -> speakerDAO.delete(topic));
    }

    public void deleteAllSpeakers(){
        speakerDAO.deleteAll();
    }
}
