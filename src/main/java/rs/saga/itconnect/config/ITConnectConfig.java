package rs.saga.itconnect.config;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.web.servlet.support.SpringBootServletInitializer;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.PropertySource;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

/**
 * Created by mmilcic on 3/23/2018.
 * Spring boot configuration
 */
@SpringBootApplication()
@PropertySource(value = {"classpath:application.properties"})
@ComponentScan(value = {"rs.saga.itconnect"})
@EnableJpaRepositories(value = "rs.saga.itconnect.dao")
@EntityScan("rs.saga.itconnect.entity")
public class ITConnectConfig extends SpringBootServletInitializer {

    @Override
    protected SpringApplicationBuilder configure(SpringApplicationBuilder application) {
        // Customize the application or call application.sources(...) to add sources
        // Since our example is itself a @Configuration class we actually don't
        // need to override this method.
        return application;
    }

    public static void main(String[] args) {
        SpringApplication.run(ITConnectConfig.class, args);
    }

}
