package rs.saga.itconnect.exception;

public class TopicNotExistException extends RuntimeException {

    private String message;

    public TopicNotExistException(String message){
        this.message = message;
    }

    @Override
    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
