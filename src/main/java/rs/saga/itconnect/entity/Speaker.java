package rs.saga.itconnect.entity;
import javax.persistence.*;

/**
 * Created by mmilcic on 3/16/2018.
 */

@Entity
public class Speaker {

    @Id
    @GeneratedValue
    private Integer id;

    private String name;
    private String surname;
    private String organization;

    @ManyToOne
    @JoinColumn(name = "TOPIC_ID")
    private Topic topic;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public String getOrganization() {
        return organization;
    }

    public void setOrganization(String organization) {
        this.organization = organization;
    }

    public Topic getTopic() {
        return topic;
    }

    public void setTopic(Topic topic) {
        this.topic = topic;
    }
}
