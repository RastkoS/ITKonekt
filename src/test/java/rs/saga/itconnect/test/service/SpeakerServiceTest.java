package rs.saga.itconnect.test.service;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import rs.saga.itconnect.config.ITConnectConfig;
import rs.saga.itconnect.entity.Speaker;
import rs.saga.itconnect.entity.Topic;
import rs.saga.itconnect.exception.SpeakerNotExistException;
import rs.saga.itconnect.service.ITSpeakerService;
import rs.saga.itconnect.test.builder.SpeakerBuilder;
import rs.saga.itconnect.test.builder.TopicBuilder;

import static org.junit.Assert.assertEquals;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = ITConnectConfig.class)
public class SpeakerServiceTest {

    @Autowired
    private ITSpeakerService speakerService;

    private Speaker speaker;
    private Topic topic;
    @Before
    public void loadDataTopic(){
        topic = new TopicBuilder().topic("title", "description", "category");
    }
    @Before
    public void loadData(){
        speaker = new SpeakerBuilder().speaker("name", "surname", "organization",topic );
        speakerService.saveOrUpdateSpeaker(speaker);
    }

    @Test(expected = SpeakerNotExistException.class)
    public void testGetByIdException(){
        speakerService.getSpeakerByID(0);
    }

    @Test
    public void testGetById(){
        Speaker dbSpeaker = speakerService.getSpeakerByID(speaker.getId());
        assertEquals(dbSpeaker.getName(), speaker.getName());
        assertEquals(dbSpeaker.getSurname(), speaker.getSurname());
        assertEquals(dbSpeaker.getOrganization(), speaker.getOrganization());
    }
}
