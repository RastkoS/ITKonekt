package rs.saga.itconnect.test.rest;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.ResultActions;
import rs.saga.itconnect.config.ITConnectConfig;
import rs.saga.itconnect.dao.SpeakerDAO;
import rs.saga.itconnect.dao.TopicDAO;
import rs.saga.itconnect.entity.Speaker;
import rs.saga.itconnect.entity.Topic;
import rs.saga.itconnect.test.builder.SpeakerBuilder;
import rs.saga.itconnect.test.builder.TopicBuilder;

import static org.hamcrest.Matchers.containsString;
import static org.hamcrest.Matchers.hasSize;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@SuppressWarnings("Duplicates")
@RunWith(SpringRunner.class)
@SpringBootTest(classes = ITConnectConfig.class)
@AutoConfigureMockMvc
public class SpeakerRestMockTest {

    @Autowired
    private MockMvc mockMvc;

    @Autowired
    private TopicDAO topicDAO;

    @Autowired
    private SpeakerDAO speakerDAO;

    @Autowired
    ObjectMapper objectMapper;

    private Speaker speaker1;

    @Before
    public void loadData(){
        speaker1 = new Speaker();
        Topic topic1 = new Topic();
        topic1.setTitle("title");
        topic1.setDescription("description");
        topic1.setCategory("categoty");
        topicDAO.save(topic1);
        speaker1.setName("Name");
        speaker1.setSurname("Surname");
        speaker1.setOrganization("Organization");
        speaker1.setTopic(topic1);
        speakerDAO.save(speaker1);

        Topic topic2 = new Topic();
        topic2.setTitle("title");
        topic2.setDescription("description");
        topic2.setCategory("categoty");
        topicDAO.save(topic2);
        Speaker speaker2 = new Speaker();
        speaker2.setName("Name2");
        speaker2.setSurname("Surname2");
        speaker2.setOrganization("Organization2");
        speaker2.setTopic(topic2);
        speakerDAO.save(speaker2);
    }

    @Test
    public void testGetSpeaker() throws Exception {
        this.mockMvc.perform(get("/speaker/id/" + speaker1.getId())).andDo(print()).andExpect(status().isOk())
                .andExpect(content().string(containsString("Name")));

        MvcResult mvcResult = this.mockMvc.perform(get("/speaker/id/" + + speaker1.getId())).andDo(print()).andReturn();
        mvcResult.getResponse().getStatus();

        ResultActions resultActions= this.mockMvc.perform(get("/speaker/id/" + + speaker1.getId()));
        resultActions.andExpect(jsonPath("$.name").value("Name"));

    }

    @Test
    public void testGetSpeakers() throws Exception {
        this.mockMvc.perform(get("/speaker/all")).andDo(print()).andExpect(status().isOk())
                .andExpect(content().string(containsString("Name")));

        MvcResult mvcResult = this.mockMvc.perform(get("/speaker/all")).andDo(print()).andReturn();
        mvcResult.getResponse().getStatus();

        ResultActions resultActions= this.mockMvc.perform(get("/speaker/all"));
        resultActions.andExpect(jsonPath("$", hasSize(2)));

    }

    @Test
    public void testDeleteSpeaker() throws Exception {
        this.mockMvc.perform(delete("/speaker/delete/" + + speaker1.getId())).andDo(print()).andExpect(status().isOk());

        MvcResult mvcResult = this.mockMvc.perform(delete("/speaker/delete/" + + speaker1.getId())).andDo(print()).andReturn();
        mvcResult.getResponse().getStatus();

        ResultActions resultActions= this.mockMvc.perform(delete("/speaker/delete/" + + speaker1.getId()));
        resultActions.andExpect(body -> {});

    }

    @Test
    public void testUpdateSpeaker() throws Exception {
        speaker1.setName("Name3");
        String jsonInString = objectMapper.writerWithDefaultPrettyPrinter().writeValueAsString(speaker1);
        this.mockMvc.perform(put("/speaker/update").contentType(MediaType.APPLICATION_JSON).content(jsonInString)).andDo(print()).andExpect(status().isOk());

        MvcResult mvcResult = this.mockMvc.perform(put("/speaker/update").contentType(MediaType.APPLICATION_JSON).content(jsonInString)).andDo(print()).andReturn();
        mvcResult.getResponse().getStatus();

        ResultActions resultActions= this.mockMvc.perform(put("/speaker/update").contentType(MediaType.APPLICATION_JSON).content(jsonInString));
        resultActions.andExpect(jsonPath("$.name").value("Name3"));

    }

    @Test
    public void testInsertSpeaker() throws Exception {
        Topic topic3 = new TopicBuilder().topic("title3", "description3", "category3");
        topicDAO.save(topic3);
        Speaker speaker3 = new SpeakerBuilder().speaker("name3", "surname3", "organization3", topic3);
        String jsonInString = objectMapper.writerWithDefaultPrettyPrinter().writeValueAsString(speaker3);
        this.mockMvc.perform(post("/speaker/new").contentType(MediaType.APPLICATION_JSON).content(jsonInString)).andDo(print()).andExpect(status().isOk());

        MvcResult mvcResult = this.mockMvc.perform(post("/speaker/new").contentType(MediaType.APPLICATION_JSON).content(jsonInString)).andDo(print()).andReturn();
        mvcResult.getResponse().getStatus();

        ResultActions resultActions= this.mockMvc.perform(post("/speaker/new").contentType(MediaType.APPLICATION_JSON).content(jsonInString));
        resultActions.andExpect(jsonPath("$.name").value("name3"));

    }

    @After
    public void tearDownData(){
        speakerDAO.deleteAll();
    }

}
