package rs.saga.itconnect.test.rest;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.junit4.SpringRunner;
import rs.saga.itconnect.config.ITConnectConfig;
import rs.saga.itconnect.dao.SpeakerDAO;
import rs.saga.itconnect.dao.TopicDAO;
import rs.saga.itconnect.entity.Speaker;
import rs.saga.itconnect.entity.Topic;
import rs.saga.itconnect.test.builder.TopicBuilder;

import java.util.Collection;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;

@SuppressWarnings("Duplicates")
@RunWith(SpringRunner.class)
@SpringBootTest(classes = ITConnectConfig.class, webEnvironment  = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class SpeakerRestTest {
    @LocalServerPort
    private int port;

    @Autowired
    private TestRestTemplate restTemplate;

    @Autowired
    private SpeakerDAO speakerDAO;

    @Autowired
    private TopicDAO topicDAO;

    private Speaker speaker1;

    @Before
    public void loadData(){
        speaker1 = new Speaker();
        Topic topic1 = new Topic();
        topic1.setTitle("title");
        topic1.setDescription("description");
        topic1.setCategory("categoty");
        topicDAO.save(topic1);
        speaker1.setName("Name");
        speaker1.setSurname("Surname");
        speaker1.setOrganization("Organization");
        speaker1.setTopic(topic1);
        speakerDAO.save(speaker1);

        Topic topic2 = new Topic();
        topic2.setTitle("title2");
        topic2.setDescription("description2");
        topic2.setCategory("categoty2");
        topicDAO.save(topic2);
        Speaker speaker2 = new Speaker();
        speaker2.setName("Name2");
        speaker2.setSurname("Surname2");
        speaker2.setOrganization("Organization2");
        speaker2.setTopic(topic2);
        speakerDAO.save(speaker2);

    }

    @Test
    public void testGetSpeakerNoDataExist() {

        ResponseEntity<Speaker> response = this.restTemplate.getForEntity("http://localhost:" + port + "/speaker/id/0", Speaker.class);
        assertEquals("status not valid", HttpStatus.NOT_FOUND, response.getStatusCode());
        Speaker speaker = response.getBody();
        assertNull(speaker.getName());
    }

    @Test
    public void testGetSpeakerDataExist() {

        ResponseEntity<Speaker> responseSpeaker = this.restTemplate.getForEntity("http://localhost:" + port + "/speaker/id/" + speaker1.getId(), Speaker.class);
        assertEquals("status not valid", HttpStatus.OK, responseSpeaker.getStatusCode());
        Speaker speaker = responseSpeaker.getBody();
        assertNotNull(speaker.getName());

        ResponseEntity<Collection> responseCollection = this.restTemplate.getForEntity("http://localhost:" + port + "/speaker/all", Collection.class);
        assertEquals("status not valid", HttpStatus.OK, responseCollection.getStatusCode());
        assertEquals("size not valid", 2, responseCollection.getBody().size());
    }

    @Test
    public void testInsertTopic() {
        Topic topic3 = new TopicBuilder().topic("title3", "description3", "category");
        topicDAO.save(topic3);
        Speaker speaker3 = new Speaker();
        speaker3.setName("Name2");
        speaker3.setSurname("Surname2");
        speaker3.setOrganization("Organization2");
        speaker3.setTopic(topic3);
        ResponseEntity<Speaker> response = this.restTemplate.postForEntity("http://localhost:" + port + "/speaker/new", speaker3, Speaker.class);
        assertEquals("status not valid", HttpStatus.OK, response.getStatusCode());
        Speaker speaker = response.getBody();
        assertEquals("names not equals", speaker3.getName(), speaker.getName());
    }

    @Test
    public void testUpdateTopic() {
        speaker1.setName("newName");
        this.restTemplate.put("http://localhost:" + port + "/speaker/update", speaker1);

        ResponseEntity<Speaker> responseSpeaker = this.restTemplate.getForEntity("http://localhost:" + port + "/speaker/id/" + speaker1.getId(), Speaker.class);
        assertEquals("status not valid", HttpStatus.OK, responseSpeaker.getStatusCode());
        Speaker speaker = responseSpeaker.getBody();
        assertNotNull(speaker.getName());
    }

    @Test
    public void testDeleteTopic() {
        this.restTemplate.delete("http://localhost:" + port + "/speaker/delete/"+ + speaker1.getId());
        ResponseEntity<Speaker> response = this.restTemplate.getForEntity("http://localhost:" + port + "/speaker/id/"+ + speaker1.getId(), Speaker.class);
        assertEquals("status not valid", HttpStatus.NOT_FOUND, response.getStatusCode());
        Speaker speaker = response.getBody();
        assertNull(speaker.getName());
    }

    @After
    public void tearDownData(){
        speakerDAO.deleteAll();
    }

}
