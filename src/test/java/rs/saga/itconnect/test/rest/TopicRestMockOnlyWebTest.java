package rs.saga.itconnect.test.rest;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.AutoConfigurationExcludeFilter;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.context.TypeExcludeFilter;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.FilterType;
import org.springframework.http.MediaType;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.ResultActions;
import rs.saga.itconnect.entity.Topic;
import rs.saga.itconnect.rest.TopicREST;
import rs.saga.itconnect.service.ITopicService;
import rs.saga.itconnect.test.builder.TopicBuilder;

import static org.hamcrest.Matchers.containsString;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@RunWith(SpringRunner.class)
@WebMvcTest(TopicREST.class)
@ContextConfiguration
public class TopicRestMockOnlyWebTest {

    @Autowired
    private MockMvc mockMvc;

    @Autowired
    ObjectMapper objectMapper;

    @MockBean
    private ITopicService topicService;

    @Test
    public void testGetTopic() throws Exception {

        when(topicService.getTopicByID(1)).thenReturn(new TopicBuilder().topic("title", "description", "category"));

        MvcResult mvcResult = this.mockMvc.perform(get("/topic/id/1"))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(content().string(containsString("title"))).andReturn();

        ResultActions resultActions= this.mockMvc.perform(get("/topic/id/1"));
//        https://github.com/json-path/JsonPath
        resultActions.andExpect(jsonPath("$.title").value("title"));

    }

    @Test
    public void testInsertTopic() throws Exception {
        Topic topic = new TopicBuilder().topicWithID(3, "title", "description", "category");
        String jsonInString = objectMapper.writerWithDefaultPrettyPrinter().writeValueAsString(topic);

        this.mockMvc.perform(post("/topic/new").contentType(MediaType.APPLICATION_JSON).content(jsonInString)).andDo(print()).andExpect(status().isOk())
                .andExpect(content().string(containsString("title")));

        MvcResult mvcResult = this.mockMvc.perform(post("/topic/new").contentType(MediaType.APPLICATION_JSON).content(jsonInString)).andDo(print()).andReturn();
        mvcResult.getResponse().getStatus();

        ResultActions resultActions= this.mockMvc.perform(post("/topic/new").contentType(MediaType.APPLICATION_JSON).content(jsonInString));
        resultActions.andExpect(jsonPath("$.title").value("title"));

    }


    @Configuration
    @EnableAutoConfiguration
    @ComponentScan(basePackages = "rs.saga.itconnect", excludeFilters = {
            @ComponentScan.Filter(type = FilterType.CUSTOM, classes = TypeExcludeFilter.class),
            @ComponentScan.Filter(type = FilterType.CUSTOM, classes = AutoConfigurationExcludeFilter.class) })
    static class TestConfig {

    }

}
