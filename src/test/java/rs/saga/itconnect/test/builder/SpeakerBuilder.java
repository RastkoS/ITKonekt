package rs.saga.itconnect.test.builder;

import rs.saga.itconnect.entity.Speaker;
import rs.saga.itconnect.entity.Topic;

public class SpeakerBuilder {

    private Speaker model;

    public SpeakerBuilder() {
        model = new Speaker();
    }

    private SpeakerBuilder name(String name) {
        model.setName(name);
        return this;
    }

    private SpeakerBuilder surname(String surname) {
        model.setSurname(surname);
        return this;
    }

    private SpeakerBuilder organization(String organization) {
        model.setOrganization(organization);
        return this;
    }

    private SpeakerBuilder topic(Topic topic) {
        model.setTopic(topic);
        return this;
    }

    private Speaker build() {
        return model;
    }


    public Speaker speaker(String name, String surname, String organization, Topic topic) {
        return new SpeakerBuilder().name(name).surname(surname).organization(organization).topic(topic).build();
    }

}